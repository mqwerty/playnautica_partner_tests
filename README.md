# Partner tests

Набор тестов, собранных как артефакт, который будет предоставляться партнеру, чтобы он смог протеситровать поведение имплементации своей API. Тесты написаны на Node.js, на фреймворке `jest` и библиотеке `supertest`. По своей сути они очень простые.

## Quickstart

1. Установите Node.js 18.\* или выше
2. `npm install`
3. Подготовили конфиг (см ниже)
4. `npm test`

## Установка

### 1. Поставить Node.js

Node.js можете ставить любым методом, подходящим вашей системе. Мы работает с версией 18.\*, так что лучше ее поставить, но по факту и более ранние могут подойти без проблем.

Рекомендуем использовать [nvm](https://github.com/nvm-sh/nvm)

### 2. Установить зависимости

```bash
cd partner-tests && npm install
```

## Тесты

Тесты поделены на базовые и сложные (`base-cases` и `complex-cases`). В простых тестах делаются атомарные запросы по каждому эндпоинту и проверяют формат ответа, изменения в балансе игрока. В сложных тестах проверяются более сложные условия (конкуретные запросы, несколько последовательных запросов на разные эндпоинты)

## Конфиг

Для запуска тестов нужен конфиг. У него две основные цели:

1. Описать параметры системы (URL и прочее)
2. Описать игроков, используемых для тестов. Так как в тестируемом API нет никакого механизма для их создания, эта часть полностью на вашей стороне, вы должны указать просто готовые id и токены.

Мы для себя генерим пользователей в конфиг с помощью скрипта. У нас есть для этого эталонная имплементация АПИ партнера, с которой мы работает локально, и к ней добавлено несколько методов для генерации пользователей и управления их балансами (для тестов). Вы можете написать аналогичный скрипт или просто захардкодить нужные поля.

Пример конфига: `config/config.example.yaml`.
Когда подготовите свой конфиг, сохраните его как `config/config.yaml`.

## Генератор конфига

Для своей эталонной имплементации мы написали скрипт, который создает игроков и генерирует `config.yaml` на лету. Скрипт расположен в `src/bin/make-dev-players.ts`, он сгенерирует нужный конфиг и сохранит его как `config/config.yaml`

В скрипте хранится дефолтный скелет конфига + код который ходит к нам на сервер на вспомогательный API и создает игроков для теста. Вы можете переписать скрипт для генерации игроков для своей системы аналогичным образом или использовать статический конфиг.

### Конфигурация и запуск

Запуск:

```bash
npm run generate-config
```

- По умолчанию скрипт использует адрес `http://localhost:3000/api/partner` для генерации конфига
- Можно изменить переопределить через env `PARTNER_URL=http://your.domain/you/api`

### Вспомогательный API

```JSON
POST /admin/player/batch-create

{
  "idFormat": "player-id-#", // символ # будет заменен на число (1, 2, 3, ...)
  "tokenFormat": "token-#", // символ # будет заменен на число (1, 2, 3, ...)
  "balance": 10000, // Баланс каждого игрока
  "idRange": {
    "from": 1, // С какого начинать
    "to": 20 // На каком заканчивать
  }
}
```

- Создает игроков, если они не существуют
- Обновляет баланс существующим игрокам

### Параметры в конфиге

- `params.betAmount` - размер ставки, которые будут делаться на разных этапах теста
- `params.betCount` - количество ставок на игрока в тестах множественные ставок
- `params.payoutAmount` - размер выплаты
- `params.serverUrl` - URL партнера
- `params.metaFormat` - В каком формате возвращать метаданные (см. комментарии к `RequestMetaFormat`)
- `params.signatureSecret` - секрет для подписи
- `params.signatureExpirationTimeMs` - время жизни подписи
- `params.concurrency` - параметры для concurrent тестов, где делается много параллельных запросов, проверка race condition'ов и целостности данных
- `params.concurrency.groupsCount` - количество групп, каждая из которых будет делать конкуретные ставки
- `params.concurrency.totalPlayers` - количество игроков, которые будут разбиты на группы (их количество задается на предыдущем шаге) и каждый будет делать конкурентные ставки
- `params.concurrency.iterations` - кол-во итераций для теста, в котором делается множество разных ставок и выплат в хаотичном порядке. Лучше выставить это число хотя бы в 50, так как в рамках этого теста хорошо вылезают некоторые проблемы, которые простым образом не обнаружить.

- `needs.playerCount` - это необходимое кол-во игроков, которое нужно предоставить для тестирования (их кол-во должно быть не менее `params.concurrency.totalPlayers`)
- `needs.playerMinBalance` - минимальный размер баланса, который должен быть у каждого игрока (Менять значение не нужно!)
- `players` - нужно предоставить массивом список игроков (их кол-во должно соответствовать значению `needs.playerCount`)
- `customPlayers` - это игроки с особыми условиями (например забаненый игрок или игрок, у которого истек токен)

### Рекомендации по concurrency

Часть тестов рассчитаны на обнаружение коллизий при конкурентных запросах, т.к. в проде нагрузка может быть ощутимая и это надо учитывать. Но такие ошибки носят недетерминистичный характер, для их обнаружения нужна нагрузка.

По умолчанию выставлено небольшой уровень конкурентности. Когда API готово рекомендуем его значительно повысить (больше `totalPlayers` и `groupsCount`, увеличить количество повторов для этих тестов: `iterations`) и посмотреть на поведение, ошибки или отказы.

## Запуск тестов

После предоставления `config.yaml` запустить тесты можно командой.

```
npm test
```

Она последовательно прогонит все тесты на все существующие методы. Если вы хотите протестировать только часть тестов, можно воспользоваться опцией `-t`

```
npm test -- -t get-balance
```

Так же можно вывести debug output и посмотреть какие в точности запросы отправляются на сервер с помощью перемернной `DEBUG`:

```
DEBUG=http:request npm test -- -t get-balance
```
