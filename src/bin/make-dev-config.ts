import fs from 'fs';
import yaml from 'yaml';
import dotenv from 'dotenv';
import { RequestMetaFormat, YamlConfig } from '@/types';
import { configPath } from '@/lib/config';
import { PartnerApi } from '@/lib/partner-api';

dotenv.config();

(async () => {
  const serverUrl =
    process.env.PARTNER_URL || 'http://localhost:3000/api/partner';
  const signatureSecret = process.env.PARTNER_SIGNATURE_SECRET || 'MY_SECRET';
  const signatureExpirationTimeMs = Number(
    process.env.PARTNER_SIGNATURE_EXPIRATION_TIME_MS || '30000',
  );
  const currencyCode = process.env.PARTNER_DEFAULT_CURRENCY_CODE || 'RUB';
  console.log(`Regenerating config: ${configPath}`);
  console.log(`Server URL: ${serverUrl}`);
  console.log(
    `Signature: secret "${signatureSecret}", expiration time: ${signatureExpirationTimeMs} ms`,
  );

  const yamlConfig: YamlConfig = {
    params: {
      betAmount: 100,
      betCount: 10,
      payoutAmount: 200,
      serverUrl,
      signatureSecret,
      currencyCode,
      signatureExpirationTimeMs,
      metaFormat: RequestMetaFormat.Raw,
      concurrency: {
        iterations: 1,
        groupsCount: 10,
        totalPlayers: 30,
      },
    },
    needs: {
      playerCount: 30,
      playerMinBalance: 100000,
    },
    customPlayers: {
      bannedPlayer: {
        id: 'banned-player-id',
        token: 'banned-player-token',
        currency: currencyCode,
      },
      expiredTokenPlayer: {
        id: 'expired-token-player-id',
        token: 'expired-token-player-id',
        currency: currencyCode,
      },
    },
    players: [],
  };

  const partnerApi = new PartnerApi(yamlConfig.params);
  yamlConfig.players = await partnerApi.createPlayers(
    'player-id-#',
    'valid-token-#',
    yamlConfig.needs.playerMinBalance,
    { from: 1, to: yamlConfig.needs.playerCount },
  );

  fs.writeFileSync(configPath, yaml.stringify(yamlConfig), 'utf-8');
})().catch(err => {
  console.error(err);
  process.exit(1);
});
