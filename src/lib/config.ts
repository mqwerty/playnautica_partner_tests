import fs from 'fs';
import path from 'path';
import yaml from 'yaml';

import { YamlConfig, YamlConfigParams } from '@/types';

export const configPath = path.resolve(__dirname, '../../config/config.yaml');

let cachedConfig: YamlConfig | undefined;

export const getConfig = (): YamlConfig => {
  if (!cachedConfig) {
    cachedConfig = yaml.parse(fs.readFileSync(configPath, 'utf-8'));
  }
  return cachedConfig as YamlConfig;
};

export const getTestParams = (): YamlConfigParams => {
  return getConfig().params;
};
