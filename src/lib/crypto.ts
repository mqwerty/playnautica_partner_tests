import crypto from 'crypto';

const ALGORITHM = 'sha256';

export const getHmac = (secret: string, data: string): string => {
  return crypto
    .createHmac(ALGORITHM, secret)
    .update(data, 'utf8')
    .digest('hex');
};
