import {
  Bet,
  MetaBet,
  MetaPayout,
  Payout,
  PayoutSource,
  RequestMetaFormat,
} from '@/types';

export interface MetaGenerator {
  bet: (bet: Bet) => MetaBet;
  payout: (payout: Payout) => MetaPayout;
}

const DicesMetaGenerator = {
  bet: (): MetaBet => ({
    gameType: 'game_dices',
    slotType: 'num_22',
    slotFactor: 105,
    slotValues: ['4_4'],
  }),
  payout: (payout: Payout): MetaPayout => {
    let roundResult = undefined;
    if (payout.payoutSource === PayoutSource.RoundFinished) {
      roundResult = '4_4';
    } else if (payout.payoutSource === PayoutSource.RoundResultChanged) {
      roundResult = '4_5';
    }
    return {
      gameType: 'game_dices',
      roundResult,
    };
  },
};

const TableCellsMetaGenerator = {
  bet: (): MetaBet => ({
    gameType: 'game_dices',
    slotType: 'num_22',
    slotFactor: 105,
    slotValues: ['22'],
  }),
  payout: (payout: Payout): MetaPayout => {
    let roundResult = undefined;
    if (payout.payoutSource === PayoutSource.RoundFinished) {
      roundResult = '22';
    } else if (payout.payoutSource === PayoutSource.RoundResultChanged) {
      roundResult = '23';
    }
    return {
      gameType: 'game_dices',
      roundResult,
    };
  },
};

export const MetaGenerators: Record<RequestMetaFormat, MetaGenerator> = {
  [RequestMetaFormat.Raw]: DicesMetaGenerator,
  [RequestMetaFormat.TableCells]: TableCellsMetaGenerator,
};
