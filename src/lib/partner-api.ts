import _ from 'lodash';
import supertest from 'supertest';
import debug from 'debug';

import { getHmac } from './crypto';
import { Bet, Payout, RequestMetaFormat, YamlConfigPlayer } from '@/types';
import { MetaGenerators } from './partner-api-meta-generator';

const requestDebug = debug('http:request');

interface PartnerApiParams {
  serverUrl: string;
  signatureSecret: string;
  currencyCode: string;
  metaFormat?: RequestMetaFormat;
}

export class PartnerApi {
  private _signatureSecret;
  private _request;
  private _currencyCode;
  private _metaGenerator;

  constructor(params: PartnerApiParams) {
    this._signatureSecret = params.signatureSecret;
    this._request = supertest(params.serverUrl);
    this._currencyCode = params.currencyCode;
    this._metaGenerator =
      MetaGenerators[params.metaFormat || RequestMetaFormat.Raw];
  }

  private post<T extends Object>(url: string, body: T, token?: string) {
    const timestamp = Date.now();
    const bodyWithTimestamp = { ...body, timestamp };
    const signature = getHmac(
      this._signatureSecret,
      JSON.stringify(bodyWithTimestamp),
    );

    const headers = {
      'X-PL-Signature': signature,
      ...(token ? { Authorization: `Bearer ${token}` } : {}),
    };

    if (requestDebug.enabled) {
      // Depth is broken for this case, so we instead stringify it manually
      requestDebug(
        `POST ${url}`,
        JSON.stringify({ headers, body: bodyWithTimestamp }, null, 2),
      );
    }

    return this._request.post(url).set(headers).send(bodyWithTimestamp);
  }

  getPlayerBalance(token: string, playerId: string) {
    return this._request
      .get('/balance')
      .set('Authorization', `Bearer ${token}`)
      .set('meta', 'METHOD getPlayerBalance')
      .query({ playerId });
  }

  createBet(token: string, bet: Bet) {
    _.forEach(bet.bets, betConfig => {
      if (!betConfig.meta) {
        betConfig.meta = this._metaGenerator.bet(bet);
      }
    });
    return this.post('/bet', bet, token);
  }

  makePayout(payout: Payout) {
    if (!payout.meta) {
      payout.meta = this._metaGenerator.payout(payout);
    }
    return this.post('/payout', payout);
  }

  getHealth() {
    return this._request.get('/health');
  }

  createPlayers = async (
    idFormat: string,
    tokenFormat: string,
    balance: number,
    idRange: { from: number; to: number },
  ): Promise<Array<YamlConfigPlayer>> => {
    await this._request
      .post('/admin/player/batch-create')
      .send({
        idFormat,
        tokenFormat,
        balance,
        idRange,
      })
      .expect(200);

    return _.range(idRange.from, idRange.to + 1).map(id => ({
      id: idFormat.replace('#', `${id}`),
      token: tokenFormat.replace('#', `${id}`),
      currency: this._currencyCode,
    }));
  };
}
