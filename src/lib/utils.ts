import { customAlphabet } from 'nanoid';
import jwt from 'jsonwebtoken';

import { YamlConfigJWT, YamlConfigPlayer } from '@/types';
import { getConfig } from './config';

const ALPHABET =
  '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
const ID_SUFFIX_SIZE = 20;
const nanoid = customAlphabet(ALPHABET, ID_SUFFIX_SIZE);

export const makeId = (prefix = 'player') => {
  return `${prefix}-${nanoid()}`;
};

export const DATETIME_REGEX = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/;

export const getPlayers = (count: number): Array<YamlConfigPlayer> => {
  const jwtConfig = getConfig().params.jwt;
  return getConfig()
    .players.slice(0, count)
    .map(p => {
      p.token = jwtConfig ? generateJWT(p.id, jwtConfig) : p.token;
      p.id = String(p.id);
      return p;
    });
};

export const generateJWT = (playerId: string, config: YamlConfigJWT) => {
  const payload = {
    partnerId: config.partnerId,
    partnerPlayerId: playerId,
  };
  return jwt.sign(payload, config.secret, {
    expiresIn: config.expiresIn,
  });
};
