import { BetOperation, PayoutSource } from '@/types';
import { getPlayers, makeId } from '@/lib/utils';
import { PartnerApi } from '@/lib/partner-api';
import { getTestParams } from '@/lib/config';

describe('base-round-flow', () => {
  const testParams = getTestParams();
  const { betAmount, payoutAmount } = testParams;
  const partnerApi = new PartnerApi(testParams);

  it('makes several bets, cancel bets, and then do payout', async () => {
    const [player] = getPlayers(1);
    const balanceResponse = await partnerApi
      .getPlayerBalance(player.token, player.id)
      .expect(200);
    const balance = balanceResponse.body.balance;

    const roundId = makeId('round');
    const betIds = {
      firstBet: makeId('bet'),
      secondBet: makeId('bet'),
      cancelledBet: makeId('bet'),
    };

    await partnerApi.createBet(player.token, {
      playerId: player.id,
      roundId,
      betOperation: BetOperation.Make,
      bets: {
        [betIds.firstBet]: {
          betAmount,
        },
      },
    });

    await partnerApi.createBet(player.token, {
      playerId: player.id,
      roundId,
      betOperation: BetOperation.Make,
      bets: {
        [betIds.secondBet]: {
          betAmount,
        },
      },
    });

    await partnerApi.createBet(player.token, {
      playerId: player.id,
      roundId,
      betOperation: BetOperation.Make,
      bets: {
        [betIds.cancelledBet]: {
          betAmount,
        },
      },
    });

    await partnerApi.createBet(player.token, {
      playerId: player.id,
      roundId,
      betOperation: BetOperation.Cancel,
      bets: {
        [betIds.cancelledBet]: {
          betAmount,
        },
      },
    });

    await partnerApi.makePayout({
      roundId,
      payoutId: makeId('payout'),
      payoutSource: PayoutSource.RoundFinished,
      betPayouts: {
        [betIds.secondBet]: {
          playerId: player.id,
          amount: payoutAmount,
        },
      },
    });

    const { body } = await partnerApi.getPlayerBalance(player.token, player.id);
    const expectedBalance = balance - 2 * betAmount + payoutAmount;

    expect(body.balance).toEqual(expectedBalance);
  });
});
