import { DATETIME_REGEX, getPlayers, makeId } from '@/lib/utils';
import { BetOperation } from '@/types';
import { PartnerApi } from '@/lib/partner-api';
import { getTestParams } from '@/lib/config';

describe('cancel-bet', () => {
  const testParams = getTestParams();
  const partnerApi = new PartnerApi(testParams);

  const [{ id, token }] = getPlayers(1);

  let balance: number;
  let roundId: string;
  let betId: string;
  const { betAmount } = testParams;

  beforeEach(async () => {
    roundId = makeId('round');
    betId = makeId('bet');
    const balanceResponse = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    balance = balanceResponse.body.balance;
  });

  it('cancels bet', async () => {
    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Cancel,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      balance,
      rejectedBets: {},
    });

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(balance);
  });

  it('cancels multiple bets at once', async () => {
    const [firstBetId, secondBetId, thirdBetId] = [
      makeId('bet'),
      makeId('bet'),
      makeId('bet'),
    ];
    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [firstBetId]: {
            betAmount,
          },
          [secondBetId]: {
            betAmount,
          },
          [thirdBetId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Cancel,
        bets: {
          [firstBetId]: {
            betAmount,
          },
          [secondBetId]: {
            betAmount,
          },
          [thirdBetId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      balance,
      rejectedBets: {},
    });

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(balance);
  });

  it('accepts cancel bet if bet does not exists', async () => {
    const { body } = await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Cancel,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      balance,
      rejectedBets: {},
    });

    const actualBalance = await partnerApi.getPlayerBalance(token, id);
    expect(actualBalance.body.balance).toEqual(balance);
  });

  it('throws if making bet with invalid token', async () => {
    await partnerApi
      .createBet('invalid-token', {
        playerId: id,
        roundId,
        betOperation: BetOperation.Cancel,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(403);
  });

  it('throws if making bet for non-existing player', async () => {
    await partnerApi
      .createBet('invalid-token', {
        playerId: 'invalid-player-id',
        roundId,
        betOperation: BetOperation.Cancel,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(403);
  });

  it('throws if cancels bet with invalid bet amount', async () => {
    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Cancel,
        bets: {
          [betId]: {
            betAmount: 'this-is-not-amount' as unknown as number,
          },
        },
      })
      .expect(400);
  });

  it('balance not changes if making multiple cancel bet operations for the same bet', async () => {
    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Cancel,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Cancel,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      balance,
      rejectedBets: {},
    });

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(balance);
  });
});
