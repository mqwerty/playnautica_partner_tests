import { DATETIME_REGEX, getPlayers, makeId } from '@/lib/utils';
import { getTestParams } from '@/lib/config';
import { BetOperation, PartnerErrorCodes, PayoutSource } from '@/types';
import { PartnerApi } from '@/lib/partner-api';

describe('do-payout', () => {
  const testParams = getTestParams();
  const partnerApi = new PartnerApi(testParams);

  const [{ id, token }] = getPlayers(1);
  let balance: number;
  let roundId: string;
  let betId: string;
  let payoutId: string;

  const { betAmount, payoutAmount } = testParams;

  beforeEach(async () => {
    roundId = makeId('round');
    betId = makeId('bet');
    payoutId = makeId('payout');
    const balanceResponse = await partnerApi.getPlayerBalance(token, id);
    balance = balanceResponse.body.balance;
  });

  it('makes payout for existing player with existing bet', async () => {
    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .makePayout({
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            playerId: id,
            amount: payoutAmount,
          },
        },
      })
      .expect(200);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {},
    });

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(
      balance - betAmount + payoutAmount,
    );
  });

  it('does not makes payout for cancelled bet', async () => {
    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Cancel,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .makePayout({
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            playerId: id,
            amount: payoutAmount,
          },
        },
      })
      .expect(207);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {
        [betId]: {
          reason: PartnerErrorCodes.BetWasCancelled,
        },
      },
    });

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(balance);
  });

  it('does not makes payout if bet does not exists', async () => {
    const { body } = await partnerApi
      .makePayout({
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            playerId: id,
            amount: payoutAmount,
          },
        },
      })
      .expect(207);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {
        [betId]: {
          reason: PartnerErrorCodes.BetNotFound,
        },
      },
    });

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(balance);
  });

  it('does not makes payout if player was not found', async () => {
    await partnerApi.createBet(token, {
      playerId: id,
      roundId,
      betOperation: BetOperation.Make,
      bets: {
        [betId]: {
          betAmount,
        },
      },
    });

    const { body } = await partnerApi
      .makePayout({
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            playerId: 'invalid-player-id',
            amount: payoutAmount,
          },
        },
      })
      .expect(207);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {
        [betId]: {
          reason: PartnerErrorCodes.PlayerNotFound,
        },
      },
    });
  });

  it('payouts only once if same payout received multiple times', async () => {
    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .makePayout({
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            playerId: id,
            amount: payoutAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .makePayout({
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            playerId: id,
            amount: payoutAmount,
          },
        },
      })
      .expect(200);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {},
    });

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(
      balance - betAmount + payoutAmount,
    );
  });

  [
    PayoutSource.RoundCancelled,
    PayoutSource.RoundFinished,
    PayoutSource.RoundResultChanged,
    PayoutSource.RoundRolledBack,
  ].forEach(payoutSource => {
    it(`accepts payout with "${payoutSource}" payout source`, async () => {
      await partnerApi
        .createBet(token, {
          playerId: id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betAmount,
            },
          },
        })
        .expect(200);

      const { body } = await partnerApi
        .makePayout({
          roundId,
          payoutId: makeId('payout'),
          payoutSource,
          betPayouts: {
            [betId]: {
              playerId: id,
              amount: payoutAmount,
            },
          },
        })
        .expect(200);

      expect(body).toEqual({
        operationId: expect.stringMatching(/^.*$/),
        acceptedAt: expect.stringMatching(DATETIME_REGEX),
        rejectedBets: {},
      });
    });
  });

  it('can make multiple payouts for same round for same bet', async () => {
    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .makePayout({
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            playerId: id,
            amount: payoutAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .makePayout({
        roundId,
        payoutId: makeId('payout'),
        payoutSource: PayoutSource.RoundResultChanged,
        betPayouts: {
          [betId]: {
            playerId: id,
            amount: -payoutAmount,
          },
        },
      })
      .expect(200);

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);

    expect(actualBalance.body.balance).toEqual(balance - betAmount);
  });

  it('may rollback after payout round with new payout with negative amount', async () => {
    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .makePayout({
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            playerId: id,
            amount: payoutAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .makePayout({
        roundId,
        payoutId: makeId('payout'),
        payoutSource: PayoutSource.RoundRolledBack,
        betPayouts: {
          [betId]: {
            playerId: id,
            amount: -betAmount,
          },
        },
      })
      .expect(200);

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);

    expect(actualBalance.body.balance).toEqual(balance);
  });
});
