import { getPlayers } from '@/lib/utils';
import { PartnerApi } from '@/lib/partner-api';
import { getTestParams } from '@/lib/config';

describe('get-balance', () => {
  const [{ id, token, currency }] = getPlayers(1);
  const partnerApi = new PartnerApi(getTestParams());

  it('balance for existing player with valid token', async () => {
    const { body } = await partnerApi.getPlayerBalance(token, id).expect(200);

    expect(body).toEqual({
      playerId: id,
      balance: expect.any(Number),
      currency: currency,
    });
  });

  it('throws if getting balance with invalid token', async () => {
    await partnerApi.getPlayerBalance('invalid-token', id).expect(403);
  });

  it('throws if getting balance for non-existing player', async () => {
    await partnerApi
      .getPlayerBalance('invalid-token', 'invalid-player-id')
      .expect(403);
  });
});
