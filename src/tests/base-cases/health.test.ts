import { getTestParams } from '@/lib/config';
import { PartnerApi } from '@/lib/partner-api';

describe('health', () => {
  const partnerApi = new PartnerApi(getTestParams());

  it('works', async () => {
    await partnerApi.getHealth().expect(200);
  });
});
