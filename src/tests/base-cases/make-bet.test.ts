import { BetOperation, PartnerErrorCodes } from '@/types';
import { DATETIME_REGEX, getPlayers, makeId } from '@/lib/utils';
import { getTestParams } from '@/lib/config';
import { PartnerApi } from '@/lib/partner-api';

describe('make-bet', () => {
  const testParams = getTestParams();
  const partnerApi = new PartnerApi(testParams);

  const [{ id, token }] = getPlayers(1);
  let balance: number;

  let roundId: string;
  let betId: string;
  const { betAmount } = testParams;

  beforeEach(async () => {
    roundId = makeId('round');
    betId = makeId('bet');
    const { body } = await partnerApi.getPlayerBalance(token, id);
    balance = body.balance;
  });

  it('makes bet', async () => {
    const { body } = await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    const expectedBalance = balance - betAmount;
    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      balance: expectedBalance,
      rejectedBets: {},
    });

    const newBalance = await partnerApi.getPlayerBalance(token, id);
    expect(newBalance.body.balance).toEqual(expectedBalance);
  });

  it('makes multiple bets at once', async () => {
    const { body } = await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [makeId('bet')]: {
            betAmount,
          },
          [makeId('bet')]: {
            betAmount,
          },
          [makeId('bet')]: {
            betAmount,
          },
        },
      })
      .expect(200);

    const expectedBalance = balance - betAmount * 3;
    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      balance: expectedBalance,
      rejectedBets: {},
    });

    const newBalance = await partnerApi.getPlayerBalance(token, id);
    expect(newBalance.body.balance).toEqual(expectedBalance);
  });

  it('throws if making bet with invalid token', async () => {
    await partnerApi
      .createBet('invalid-token', {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(403);
  });

  it('throws if making bet for non-existing player', async () => {
    await partnerApi
      .createBet('invalid-token', {
        playerId: 'invalid-player-id',
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(403);
  });

  it('throws if making bet with invalid bet amount', async () => {
    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount: 'this-is-not-amount' as unknown as number,
          },
        },
      })
      .expect(400);
  });

  it('returns multistatus if bet amount is larger than balance', async () => {
    const { body } = await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount: balance + 100,
          },
        },
      })
      .expect(207);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      balance: balance,
      rejectedBets: {
        [betId]: {
          reason: PartnerErrorCodes.NotEnoughMoney,
        },
      },
    });
  });

  it('keeps same balance if making the same bet two or more times', async () => {
    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    const expectedBalance = balance - betAmount;
    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      balance: expectedBalance,
      rejectedBets: {},
    });

    const playerBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(playerBalance.body.balance).toEqual(expectedBalance);
  });
});
