import { getTestParams } from '@/lib/config';
import supertest from 'supertest';

describe('signature', () => {
  const testParams = getTestParams();
  const request = supertest(testParams.serverUrl);
  const playerId = 'player_id';

  ['/bet', '/payout'].map(url => {
    describe(`POST ${url}`, () => {
      it('return proper error when signature missing', async () => {
        const timestamp = Date.now();
        await request.post(url).send({ timestamp }).expect(400);
      });

      it('returns error for POST with empty body', async () => {
        await request.post(url).set('X-PL-Signature', 'signature').expect(400);
      });

      it('returns error for POST without timestamp', async () => {
        await request
          .post(url)
          .set('X-PL-Signature', 'signature')
          .send({ key: 'value' })
          .expect(400);
      });

      it('returns Timestamp expired error', async () => {
        const expiredTimestamp =
          Date.now() - testParams.signatureExpirationTimeMs - 1;
        await request
          .post(url)
          .set('X-PL-Signature', 'signature')
          .send({ timestamp: expiredTimestamp })
          .expect(400);
      });

      it('returns Invalid signature error', async () => {
        const timestamp = Date.now();
        await request
          .post(url)
          .set('X-PL-Signature', 'signature')
          .send({ timestamp })
          .expect(400);
      });
    });
  });

  describe('GET /balance', () => {
    it('return error for GET request with signature', async () => {
      await request
        .get('/balance')
        .query({ playerId: playerId })
        .set('X-PL-Signature', 'signature')
        .expect(400);
    });
  });
});
