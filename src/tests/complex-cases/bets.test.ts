import { getPlayers, makeId } from '@/lib/utils';
import { getTestParams } from '@/lib/config';
import { BetOperation } from '@/types';
import { PartnerApi } from '@/lib/partner-api';

describe('bets', () => {
  const testParams = getTestParams();
  const partnerApi = new PartnerApi(testParams);

  const [{ id, token }] = getPlayers(1);
  let balance: number;
  let roundId: string;
  let betId: string;

  const { betAmount } = testParams;

  beforeEach(async () => {
    roundId = makeId('round');
    betId = makeId('bet');
    const balanceResponse = await partnerApi.getPlayerBalance(token, id);
    balance = balanceResponse.body.balance;
  });

  it('ignores make bet request if bet was canceled before', async () => {
    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Cancel,
        bets: {
          [betId]: {
            betAmount: balance + 100,
          },
        },
      })
      .expect(200);

    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount: balance + 100,
          },
        },
      })
      .expect(200);

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(balance);
  });

  it('ignores multiple make bet requests if bet was canceled before', async () => {
    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Cancel,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(balance);
  });
});
