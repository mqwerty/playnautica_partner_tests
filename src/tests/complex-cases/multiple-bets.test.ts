import _ from 'lodash';
import { DATETIME_REGEX, getPlayers, makeId } from '@/lib/utils';
import { getTestParams } from '@/lib/config';
import { BetOperation } from '@/types';
import { PartnerApi } from '@/lib/partner-api';

describe('multiple-bets', () => {
  const testParams = getTestParams();
  const partnerApi = new PartnerApi(testParams);

  const [{ id, token }] = getPlayers(1);

  let roundId: string;
  let balance: number;
  const { betAmount, betCount } = testParams;

  beforeEach(async () => {
    roundId = makeId('round');
    const balanceResponse = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    balance = balanceResponse.body.balance;
  });

  it('succesfully makes series of bets', async () => {
    let expectedBalance = balance;
    for (let i = 0; i < betCount; i++) {
      const makeBetResponse = await partnerApi
        .createBet(token, {
          playerId: id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [makeId('bet')]: {
              betAmount,
            },
          },
        })
        .expect(200);

      expectedBalance -= betAmount;
      expect(makeBetResponse.body).toEqual({
        operationId: expect.stringMatching(/^.*$/),
        acceptedAt: expect.stringMatching(DATETIME_REGEX),
        balance: expectedBalance,
        rejectedBets: {},
      });
    }

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(expectedBalance);
  });

  it('makes several concurrent bets', async () => {
    await Promise.all(
      _.range(betCount).map(() =>
        partnerApi
          .createBet(token, {
            playerId: id,
            roundId,
            betOperation: BetOperation.Make,
            bets: {
              [makeId('bet')]: {
                betAmount,
              },
            },
          })
          .expect(200),
      ),
    );

    const {
      body: { balance: actualBalance },
    } = await partnerApi.getPlayerBalance(token, id).expect(200);
    expect(actualBalance).toEqual(balance - betAmount * betCount);
  });

  it('makes several serials bets and cancels', async () => {
    const betIds = _.range(betCount).map(() => makeId('bet'));
    for (const betId of betIds) {
      await partnerApi
        .createBet(token, {
          playerId: id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betAmount,
            },
          },
        })
        .expect(200);
    }

    let expectedBalance = balance - betAmount * betCount;
    for (const betId of betIds) {
      const cancelBetResponse = await partnerApi
        .createBet(token, {
          playerId: id,
          roundId,
          betOperation: BetOperation.Cancel,
          bets: {
            [betId]: {
              betAmount,
            },
          },
        })
        .expect(200);

      expectedBalance += betAmount;
      expect(cancelBetResponse.body).toEqual({
        operationId: expect.stringMatching(/^.*$/),
        acceptedAt: expect.stringMatching(DATETIME_REGEX),
        balance: expectedBalance,
        rejectedBets: {},
      });
    }

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(balance);
  });

  it('makes several concurrent bets and cancels', async () => {
    await Promise.all(
      _.range(betCount).flatMap(() => {
        const betId = makeId('bet');
        return [
          partnerApi.createBet(token, {
            playerId: id,
            roundId,
            betOperation: BetOperation.Make,
            bets: {
              [betId]: {
                betAmount,
              },
            },
          }),
          partnerApi.createBet(token, {
            playerId: id,
            roundId,
            betOperation: BetOperation.Cancel,
            bets: {
              [betId]: {
                betAmount,
              },
            },
          }),
        ];
      }),
    );

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(balance);
  });
});
