import { getPlayers, makeId } from '@/lib/utils';
import { getTestParams } from '@/lib/config';
import { BetOperation, PartnerErrorCodes, PayoutSource } from '@/types';
import { PartnerApi } from '@/lib/partner-api';

describe('payouts', () => {
  const testParams = getTestParams();
  const partnerApi = new PartnerApi(testParams);

  const [{ id, token }] = getPlayers(1);
  let roundId: string;
  let betId: string;

  const { betAmount, payoutAmount } = testParams;

  beforeEach(async () => {
    roundId = makeId('round');
    betId = makeId('bet');
  });

  it('prevents cancelling bet after payout', async () => {
    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .makePayout({
        roundId,
        payoutId: makeId('payout'),
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            playerId: id,
            amount: payoutAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Cancel,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(207);

    expect(body.rejectedBets).toEqual({
      [betId]: {
        reason: PartnerErrorCodes.BetAlreadyPaidOut,
      },
    });
  });

  it('allows cancelling non-payouted bet after payout for round', async () => {
    const otherBetId = makeId('bet');

    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [otherBetId]: {
            betAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .makePayout({
        roundId,
        payoutId: makeId('payout'),
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          // Payout can't be empty, so we create another bet
          [otherBetId]: {
            playerId: id,
            amount: payoutAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .createBet(token, {
        playerId: id,
        roundId,
        betOperation: BetOperation.Cancel,
        bets: {
          [betId]: {
            betAmount,
          },
        },
      })
      .expect(200);
  });
});
