export enum PartnerErrorCodes {
  BetNotFound = 'bet_not_found',
  BetWasCancelled = 'bet_was_cancelled',
  PlayerNotFound = 'player_not_found',
  PlayerBanned = 'player_banned',
  NotEnoughMoney = 'not_enough_money',
  BetAlreadyPaidOut = 'bet_already_paid_out',
  // You can return it for any internal reason for cancelling the bet
  BetRejected = 'bet_rejected',
}

export enum BetOperation {
  Make = 'make',
  Cancel = 'cancel',
}

export enum PayoutSource {
  RoundFinished = 'round_finished',
  RoundCancelled = 'round_cancelled',
  RoundResultChanged = 'round_result_changed',
  RoundRolledBack = 'round_rolled_back',
}

// Meta is not strictly checked for integrity,
// but user for representation on partner's side
export enum RequestMetaFormat {
  // Meta values represented as raw values as we store them - e.g. raw dice pairs like `1_1`
  Raw = 'raw',
  // Meta values represented as dice table cell numbers, like `22` for
  // raw `4_4` value
  TableCells = 'table_cells',
}

export type MetaBet = {
  gameType: string;
  slotType: string;
  slotFactor: number;
  slotValues: string[];
};

export type MetaPayout = {
  gameType: string;
  roundResult?: string;
};

export type BetConfig = {
  betAmount: number;
  meta?: MetaBet;
};

export type Bet = {
  betOperation: BetOperation;
  roundId: string;
  playerId: string;
  // Injected by signature generator
  timestamp?: number;
  bets: Record<string, BetConfig>;
};

export type BetPayout = {
  playerId: string;
  amount: number;
};

export type Payout = {
  roundId: string;
  payoutId: string;
  payoutSource: PayoutSource;
  // Injected by signature generator
  timestamp?: number;
  meta?: MetaPayout;
  betPayouts: Record<string, BetPayout>;
};

export type YamlConfig = {
  params: YamlConfigParams;
  needs: YamlConfigNeeds;
  players: Array<YamlConfigPlayer>;
  customPlayers: YamlCustomPlayers;
};

export type YamlConfigParams = {
  signatureSecret: string;
  signatureExpirationTimeMs: number;
  betAmount: number;
  betCount: number;
  payoutAmount: number;
  serverUrl: string;
  concurrency: Concurrency;
  currencyCode: string;
  metaFormat?: RequestMetaFormat;
  jwt?: YamlConfigJWT;
};

export type YamlConfigJWT = {
  partnerId: string;
  secret: string;
  expiresIn: string;
};

export type Concurrency = {
  iterations: number;
  totalPlayers: number;
  groupsCount: number;
};

export type YamlConfigNeeds = {
  playerCount: number;
  playerMinBalance: number;
};

export type YamlCustomPlayers = {
  bannedPlayer: YamlConfigPlayer;
  expiredTokenPlayer: YamlConfigPlayer;
};

export type YamlConfigPlayer = {
  id: string;
  token: string;
  currency: string;
};
